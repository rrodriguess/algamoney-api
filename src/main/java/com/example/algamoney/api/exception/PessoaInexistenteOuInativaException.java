package com.example.algamoney.api.exception;

/**
 * Created by renato on 10/08/17.
 */
public class PessoaInexistenteOuInativaException extends RuntimeException {

    private static final long serialVersionUID = 1l;
}
