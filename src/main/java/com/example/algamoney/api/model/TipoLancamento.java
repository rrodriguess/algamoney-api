package com.example.algamoney.api.model;

/**
 * Created by renato on 08/08/17.
 */
public enum TipoLancamento {

    RECEITA,
    DESPESA
}
